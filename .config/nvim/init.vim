
" Markers:
" r - reference (here)
" p - plugins
" s - style
" i - indentation
" f - search
" m - misc settings
" k - keybinds
" o - other

" Plugins
call plug#begin('~/.nvim/plugged')
Plug 'Raimondi/delimitMate'
Plug 'ap/vim-buftabline'
Plug 'sheerun/vim-polyglot'
Plug 'terryma/vim-multiple-cursors'
Plug 'thirtythreeforty/lessspace.vim'
Plug 'vim-airline/vim-airline'
Plug 'vimwiki/vimwiki'
call plug#end()

" Style
set background=dark   " Use dark variants of color schemes
set linebreak         " Wrap at characters within 'breakat' rather than the last character that fits
set nolist            " List breaks linebreak
set relativenumber    " Show relative line numbers
set ruler             " Show row/column info
set showmatch         " Show matching braces
set termguicolors     " True colors
set textwidth=0       " ^
set wrap              " Wrap lines instead of breaking them
set wrapmargin=0      " ^
syntax on             " Syntax highlighting
colorscheme rrred

" Indentation settings
set autoindent                 " Auto indent new lines
set backspace=indent,eol,start " Backspace behavior
set expandtab                  " Spaces instead of tabs
set shiftwidth=4               " Auto-indent spaces
set smartindent                " Smart indent
set smarttab                   " Smart tabs
set softtabstop=4              " Spaces per tab

" Search settings
set hlsearch   " Highlight all search results
set ignorecase " Always case insensitive
set incsearch  " Incremental search
set smartcase  " Let vim try to guess whether we should be case sensitive.

" Load filetype plugin for autocmds
filetype plugin on

" Change <Leader> to spacebar
let mapleader ="\<Space>"

" Automatically change working directory to that of open buffer
set autochdir

" Hide buffers instead of making them inactive when their window is closed.
set hidden

" Some plugins need this to function
set nocompatible

" Paranoia
set undolevels=1000

"No sounds
set visualbell

" Keybinds

" Plugin binds, don't feel like commenting
nnoremap <F7> :NERDTreeToggle<Enter>
nnoremap <F8> :TagbarToggle<Enter>

" CTRL+Right to switch to next buffer
nnoremap <C-Right> :bnext<CR>
" CTRL+Left to switch to previous buffer
nnoremap <C-Left> :bprev<CR>
" Reload vimrc
nnoremap <Leader>vr :source $VIMRC<CR>
" Edit vimrc in new window
nnoremap <Leader>ve :vert new $VIMRC<CR>
" Disable match highlighting.
nnoremap <Leader>h :noh<CR>

" CTRL+C to copy to clipboard.
vnoremap <C-C> "+y
" CTRL+X to cut to clipboard.
vnoremap <C-X> "+d
" CTRL+V to paste from clipboard.
vnoremap <C-V> "+p
" CTRL+A to select all.
vnoremap <C-A> <Esc>ggvG$
" Quickly select text I just pasted.
nnoremap gV `[v`]
" Changing indent level doesn't exit visual mode.
vnoremap < <gv

vnoremap > >gv

" So I can easily save constantly while in insert mode.
inoremap <C-S> <Esc>:w<Enter>a

" Always show buffer labels
let g:buftabline_show = 2
" Enable showing buffer status in label.
let g:buftabline_indicators = 1
" Enable buffer label separators.
let g:buftabline_separators = 1
" Vertical split when editing snips.
let g:UltiSnipsEditSplit="vertical"
" Tab to expand snippets.
let g:UltiSnipsExpandTrigger="<Tab>"
" CTRL+Space to advance to a jump point in snippet.
let g:UltiSnipsJumpForwardTrigger="<C-Space>"
" CTRL+Shift+Space to do the opposite. Doesn't work. Might fix some day.
let g:UltiSnipsJumpBackwardTrigger="<C-S-Space>"
" Not actually sure why I have this here, but I don't care to remove it and break something.
let g:runtimepath="~/.config/nvim"
" Set directory for snippets. For some reason not the default search path, but is the default save path.
let g:UltiSnipsSnippetsDir="~/.config/nvim/UltiSnips"
