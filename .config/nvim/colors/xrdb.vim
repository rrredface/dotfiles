set background=dark
if version > 580
	hi clear
	if exists("syntax_on")
		syntax reset
	endif
endif

let g:background = ""
let g:foreground = ""
let g:black = ""
let g:red = ""
let g:green = ""
let g:yellow = ""
let g:blue = ""
let g:magenta = ""
let g:cyan = ""
let g:white = ""
let g:ltblack = ""
let g:ltred = ""
let g:ltgreen = ""
let g:ltyellow = ""
let g:ltblue = ""
let g:ltmagenta = ""
let g:ltcyan = ""
let g:ltwhite = ""

for line in readfile($HOME."/.Xresources")
    " begin the terrible method of setting colors that can almost certainly be done way better
    let kv = split(line, ": ")
    if get(kv, 0) == "color0"
        let g:black = get(kv, 1)
    elseif get(kv, 0) == "color1"
        let g:red = get(kv, 1)
    elseif get(kv, 0) == "color2"
        let g:green = get(kv, 1)
    elseif get(kv, 0) == "color3"
        let g:yellow = get(kv, 1)
    elseif get(kv, 0) == "color4"
        let g:blue = get(kv, 1)
    elseif get(kv, 0) == "color5"
        let g:magenta = get(kv, 1)
    elseif get(kv, 0) == "color6"
        let g:cyan = get(kv, 1)
    elseif get(kv, 0) == "color7"
        let g:white = get(kv, 1)
    elseif get(kv, 0) == "color8"
        let g:ltblack = get(kv, 1)
    elseif get(kv, 0) == "color9"
        let g:ltred = get(kv, 1)
    elseif get(kv, 0) == "color10"
        let g:ltgreen = get(kv, 1)
    elseif get(kv, 0) == "color11"
        let g:ltyellow = get(kv, 1)
    elseif get(kv, 0) == "color12"
        let g:ltblue = get(kv, 1)
    elseif get(kv, 0) == "color13"
        let g:ltmagenta = get(kv, 1)
    elseif get(kv, 0) == "color14"
        let g:ltcyan = get(kv, 1)
    elseif get(kv, 0) == "color15"
        let g:ltwhite = get(kv, 1)
    elseif get(kv, 0) == "background"
        let g:background = get(kv, 1)."e5"
    elseif get(kv, 0) == "foreground"
        let g:foreground = get(kv, 1)
    endif
endfor

set t_Co=256
let g:colors_name = "xrdb"

exe 'hi Boolean guifg='.g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=248 ctermbg=NONE cterm=NONE'
exe 'hi Character guifg='.g:green .' guibg=NONE guisp=NONE gui=NONE ctermfg=248 ctermbg=NONE cterm=NONE'
exe 'hi Comment guifg='.g:ltblack .' guibg=NONE guisp=NONE gui=NONE ctermfg=239 ctermbg=NONE cterm=NONE'
exe 'hi Conditional guifg='.g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=20 ctermbg=NONE cterm=NONE'
exe 'hi Constant guifg='.g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=20 ctermbg=NONE cterm=NONE'
exe 'hi Cursor guifg='.g:background .' guibg='.g:foreground .' guisp='.g:foreground .' gui=NONE ctermfg=233 ctermbg=253 cterm=NONE'
exe 'hi CursorLineNr guifg='.g:ltblue .' guibg=NONE guisp=NONE gui=NONE ctermfg=68 ctermbg=NONE cterm=NONE'
exe 'hi Define guifg='.g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=20 ctermbg=NONE cterm=NONE'
exe 'hi Delimiter guifg='.g:foreground .' guibg=NONE guisp=NONE gui=NONE ctermfg=253 ctermbg=NONE cterm=NONE'
exe 'hi DiffAdd guifg='.g:background .' guibg='.g:green .' guisp='.g:green .' gui=NONE ctermfg=233 ctermbg=40 cterm=NONE'
exe 'hi DiffChange guifg='.g:background .' guibg='.g:yellow .' guisp='.g:yellow .' gui=NONE ctermfg=233 ctermbg=178 cterm=NONE'
exe 'hi DiffDelete guifg='.g:background .' guibg='.g:red .' guisp='.g:red .' gui=NONE ctermfg=233 ctermbg=160 cterm=NONE'
exe 'hi DiffText guifg='.g:background .' guibg='.g:yellow .' guisp='.g:yellow .' gui=NONE ctermfg=233 ctermbg=178 cterm=NONE'
exe 'hi Directory guifg='.g:foreground .' guibg=NONE guisp=NONE gui=NONE ctermfg=253 ctermbg=233 cterm=NONE'
exe 'hi ErrorMsg guifg='.g:foreground .' guibg='.g:red .' guisp='.g:red .' gui=NONE ctermfg=253 ctermbg=160 cterm=NONE'
exe 'hi Float guifg='.g:ltmagenta .' guibg=NONE guisp=NONE gui=NONE ctermfg=134 ctermbg=NONE cterm=NONE'
exe 'hi FoldColumn guifg=NONE guibg=NONE guisp=NONE gui=italic ctermfg=235 ctermbg=248 cterm=italic'
exe 'hi Folded guifg='.g:foreground .' guibg='.g:ltblack .' guisp='.g:ltblack .' gui=italic ctermfg=235 ctermbg=248 cterm=italic'
exe 'hi Function guifg='.g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=20 ctermbg=NONE cterm=NONE'
exe 'hi Identifier guifg='.g:foreground .' guibg=NONE guisp=NONE gui=NONE ctermfg=253 ctermbg=NONE cterm=NONE'
exe 'hi IncSearch guifg='.g:yellow .' guibg=NONE guisp=NONE gui=NONE ctermfg=178 ctermbg=NONE cterm=NONE'
exe 'hi Include guifg='.g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=20 ctermbg=NONE cterm=NONE'
exe 'hi Keyword guifg='g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=20 ctermbg=NONE cterm=NONE'
exe 'hi LineNr guifg='.g:ltblack .' guibg=NONE guisp=NONE gui=NONE ctermfg=239 ctermbg=NONE cterm=NONE'
exe 'hi Macro guifg='.g:cyan .' guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE'
exe 'hi MatchParen guifg='g:ltcyan .' guibg='.g:background .' guisp=NONE gui=bold ctermfg=1 ctermbg=NONE cterm=bold'
exe 'hi ModeMsg guifg='.g:foreground .' guibg='.g:background .' guisp='.g:background .' gui=bold ctermfg=253 ctermbg=233 cterm=bold'
exe 'hi MoreMsg guifg='.g:ltblue .' guibg=NONE guisp=NONE gui=bold ctermfg=68 ctermbg=NONE cterm=bold'
exe 'hi NonText guifg='.g:ltblack .' guibg=NONE guisp=NONE gui=italic ctermfg=239 ctermbg=NONE cterm=NONE'
exe 'hi Normal guifg='.g:foreground .' guibg='.g:background .' guisp='.g:background .' gui=NONE ctermfg=253 ctermbg=233 cterm=NONE'
exe 'hi Number guifg='.g:ltmagenta .' guibg=NONE guisp=NONE gui=NONE ctermfg=134 ctermbg=NONE cterm=NONE'
exe 'hi Operator guifg='.g:foreground .' guibg=NONE guisp=NONE gui=NONE ctermfg=253 ctermbg=NONE cterm=NONE'
exe 'hi PMenu guifg='.g:background .' guibg='.g:ltblack .' guisp='.g:ltblack .' gui=NONE ctermfg=233 ctermbg=239 cterm=NONE'
exe 'hi PMenuSbar guifg='.g:foreground .' guibg='.g:foreground .' guisp='.g:foreground .' gui=NONE ctermfg=253 ctermbg=253 cterm=NONE'
exe 'hi PMenuSel guifg='.g:background .' guibg='.g:ltblue .' guisp='.g:ltblue .' gui=NONE ctermfg=233 ctermbg=68 cterm=NONE'
exe 'hi PMenuThumb guifg='.g:foreground .' guibg='.g:background .' guisp='.g:background .' gui=NONE ctermfg=253 ctermbg=233 cterm=NONE'
exe 'hi PreCondit guifg='.g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=20 ctermbg=NONE cterm=NONE'
exe 'hi PreProc guifg='.g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=20 ctermbg=NONE cterm=NONE'
exe 'hi Question guifg='g:foreground .' guibg='.g:background .' guisp='.g:background .' gui=NONE ctermfg=NONE ctermbg=233 cterm=NONE'
exe 'hi Repeat guifg='g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=20 ctermbg=NONE cterm=NONE'
exe 'hi Search guifg='g:yellow .' guibg=NONE guisp=NONE gui=bold ctermfg=178 ctermbg=NONE cterm=bold'
exe 'hi SignColumn guifg=NONE guibg=NONE guisp=NONE gui=NONE ctermfg=235 ctermbg=60 cterm=NONE'
exe 'hi Special guifg='g:green .' guibg=NONE guisp=NONE gui=NONE ctermfg=40 ctermbg=NONE cterm=NONE'
exe 'hi SpecialChar guifg='g:green .' guibg=NONE guisp=NONE gui=NONE ctermfg=40 ctermbg=NONE cterm=NONE'
exe 'hi SpecialComment guifg='.g:ltblack .' guibg=italic guisp=NONE gui=italic ctermfg=239 ctermbg=NONE cterm=italic'
exe 'hi SpecialKey guifg='.g:ltwhite .' guibg=NONE guisp=NONE gui=italic ctermfg=66 ctermbg=NONE cterm=NONE'
exe 'hi SpellBad guifg=NONE guibg=NONE guisp='.g:red .' gui=undercurl ctermfg=NONE ctermbg=NONE cterm=undercurl'
exe 'hi SpellCap guifg=NONE guibg=NONE guisp='.g:yellow .' gui=undercurl ctermfg=NONE ctermbg=NONE cterm=undercurl'
exe 'hi SpellLocal guifg=NONE guibg=NONE guisp='.g:yellow .' gui=undercurl ctermfg=NONE ctermbg=NONE cterm=undercurl'
exe 'hi SpellRare guifg=NONE guibg=NONE guisp='.g:cyan.' gui=undercurl ctermfg=NONE ctermbg=NONE cterm=undercurl'
exe 'hi Statement guifg='.g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=20 ctermbg=NONE cterm=NONE'
exe 'hi StatusLine guifg='.g:background .' guibg='g:ltblue .' guisp='.g:ltblue .' gui=bold ctermfg=233 ctermbg=68 cterm=bold'
exe 'hi StatusLineNC guifg='.g:background .' guibg='g:ltblack .' guisp='.g:ltblack .' gui=bold ctermfg=233 ctermbg=239 cterm=bold'
exe 'hi String guifg='.g:green .' guibg=NONE guisp=NONE gui=NONE ctermfg=40 ctermbg=NONE cterm=NONE'
exe 'hi Structure guifg='.g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=20 ctermbg=NONE cterm=NONE'
exe 'hi TabLine guifg='.g:background .' guibg='.g:ltblack .' guisp='.g:ltblack .' gui=bold ctermfg=233 ctermbg=239 cterm=bold'
exe 'hi TabLineFill guifg='.g:background .' guibg='.g:ltblack .' guisp='.g:ltblack .' gui=bold ctermfg=233 ctermbg=239 cterm=bold'
exe 'hi TabLineSel guifg='g:background .' guibg='.g:ltblue .' guisp='.g:ltblue .' gui=bold ctermfg=233 ctermbg=68 cterm=bold'
exe 'hi Title guifg='.g:foreground .' guibg=NONE guisp=NONE gui=bold ctermfg=253 ctermbg=NONE cterm=bold'
exe 'hi Todo guifg='.g:ltyellow .' guibg=NONE guisp=NONE gui=NONE ctermfg=179 ctermbg=NONE cterm=NONE'
exe 'hi Type guifg='g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=20 ctermbg=NONE cterm=NONE'
exe 'hi Typedef guifg='g:blue .' guibg=NONE guisp=NONE gui=NONE ctermfg=20 ctermbg=NONE cterm=NONE'
exe 'hi Visual guifg='.g:background .' guibg='.g:foreground .' guisp='.g:foreground .' gui=NONE ctermfg=233 ctermbg=253 cterm=NONE'
exe 'hi VisualNOS guifg='.g:background .' guibg='.g:foreground .' guisp='.g:foreground .' gui=underline ctermfg=233 ctermbg=253 cterm=underline'
exe 'hi WarningMsg guifg='.g:red .' guibg='.g:background .' guisp='.g:background .' gui=NONE ctermfg=160 ctermbg=233 cterm=NONE'
